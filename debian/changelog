pybluez (0.23-5) unstable; urgency=medium

  * Team upload.
  * Fix Compatibility with 3.10. (Closes: #1010977)
    - Add d/patches/Use-Py_ssize_t-when-parsing-buffer-length-fix-426-42.patch
  * Update d/copyright:
    - Fix superfluous-file-pattern.
    - Add myself's copyright.
  * Update d/changelog
    - Remove empty line.
  * Update d/control
    - Use debhelper-compat instead of debhelper, and update to 13.
    - Bump Standards-Version to 4.6.0.1
    - Add 'Rules-Requires-Root: no'
  * Update d/watch
    - Update to v4.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sat, 28 May 2022 14:01:31 +0900

pybluez (0.23-4) unstable; urgency=medium

  * Team upload.
  * Fix FTBFS error in PyBluez setup command (Closes: #997640)
    - Add d/patches/Removed-2to3-command.patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 09 Nov 2021 08:42:15 +0900

pybluez (0.23-3) unstable; urgency=medium

  * Add python3-gattlib to Depends. (Closes: #930990)

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 09 Feb 2021 16:17:00 +0900

pybluez (0.23-2) unstable; urgency=medium

  * Team upload.
  [ Nobuhiro Iwamatsu ]
  * Remove autopkgtest for python2. (Closes: #937394)

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Wed, 23 Dec 2020 16:44:40 +0900

pybluez (0.23-1) unstable; urgency=medium

  * Team upload.
  [ Nobuhiro Iwamatsu ]
  * New upstream release (0.23).

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Sun, 29 Mar 2020 07:59:24 +0900

pybluez (0.22+really0.22-2) experimental; urgency=medium

  * Remove Python2 support (Closes: #937394)

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Fri, 18 Oct 2019 16:59:51 +0900

pybluez (0.22+really0.22-1) unstable; urgency=medium

  * Team upload.
  [ Nobuhiro Iwamatsu ]
  * Bump Standards-Version to 4.2.1.

  [ Diane Trout ]
  * Update the source to 0.22 For some reason the upstream source was
    never updated from 0.18 even though the Debian version number
    incremented. (Closes: #839100)
  * Remove duplicated Maintainer: tag
  * Update debhelper compat to version 11
  * Use pybuild to build the package
  * Build for Python 3. (Closes: #787850)
  * Fix hardening-no-bindnow warning by enabling hardening flags
  * Drop archaic Python 2 minimum version (2.6.6-3~)
  * Convert debian/copyright to machine parsable format
  * Add 3.0 (quilt) debian/source/format
  * Add minimal autopkgtest test

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Thu, 13 Sep 2018 07:23:23 +0900

pybluez (0.22-2) unstable; urgency=medium

  * Team upload.
  * Change maintainer address. (Closes: #899952)
  * Update Vcs-Git and Vcs-Browser.
  * Update Standards-Version to 4.1.5.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 21 Aug 2018 17:16:43 +0900

pybluez (0.22-1) unstable; urgency=medium

  * Team upload.
  [ Nobuhiro Iwamatsu ]
  * Update to 0.22.
  * Update Homepage field. (Closes: #787851)
  * Update watch file.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 08 Dec 2015 23:56:39 +0900

pybluez (0.20-1) unstable; urgency=medium

  * Team upload.
  [ Nobuhiro Iwamatsu ]
  * New upstream release.
  * Update to s-v 3.9.5, no changes required

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Thu, 12 Jun 2014 11:45:19 +0900

pybluez (0.18-2) unstable; urgency=low

  * Team upload.
  [ Nobuhiro Iwamatsu ]
  * Update to s-v 3.9.3, no changes required
  * Added build-arch and build-indep targets to debian/rules.

  [ Arthur de Jong ]
  * Moved packaging from python-central to dh_python2 (Closes: #616958).
  * Drop Conflicts/Replaces on ancient versions which never shipped in a
    stable release.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Thu, 08 Mar 2012 13:25:07 +0900

pybluez (0.18-1) unstable; urgency=low

  [ Albert Huang ]
  * New upstream release

  [ Filippo Giunchedi ]
  * Add ${misc:Depends} to Depends of python-bluez for debhelper

 -- Filippo Giunchedi <filippo@debian.org>  Wed, 06 Jan 2010 09:48:48 +0100

pybluez (0.17-1) unstable; urgency=low

  [ Albert Huang ]
  * New upstream release (Closes: #533719)
  * Update to s-v 3.8.3, no changes required

  [ Filippo Giunchedi ]
  * change vcs- fields to point to trunk
  * remove debian uupdate from watchfile, useless with svn

 -- Filippo Giunchedi <filippo@debian.org>  Mon, 19 Oct 2009 20:31:17 +0100

pybluez (0.16-1) unstable; urgency=low

  [ Albert Huang ]
  * New upstream release
  * fix debian/copyright to indicate GPL >= 2 instead of GPL == 2

  [ Filippo Giunchedi ]
  * Update to s-v 3.8.0, no changes required

 -- Filippo Giunchedi <filippo@debian.org>  Mon, 16 Feb 2009 20:33:40 +0100

pybluez (0.15-1) unstable; urgency=low

  [ Albert Huang ]
  * New upstream release

  [ Filippo Giunchedi ]
  * Fix spelling python -> Python
  * Update to s-v 3.7.3, no changes required

 -- Filippo Giunchedi <filippo@debian.org>  Sun, 10 Feb 2008 11:25:59 +0100

pybluez (0.14-1) unstable; urgency=low

  [ Albert Huang ]
  * New upstream release (closes: #446122)

  [ Filippo Giunchedi ]
  * Changed VCS-{Svn,Browser} fields plus Homepage added

 -- Filippo Giunchedi <filippo@debian.org>  Wed, 21 Nov 2007 19:11:32 +0100

pybluez (0.13-1) unstable; urgency=low

  * New upstream release
  * add vcs field to control

 -- Filippo Giunchedi <filippo@debian.org>  Wed, 10 Oct 2007 21:49:53 +0200

pybluez (0.9.2-1) unstable; urgency=low

  [ Albert Huang ]
  * New upstream release

  [ Filippo Giunchedi ]
  * Complete libbluetooth2-dev -> libbluetooth-dev transition
  * Correct Maintainers name

 -- Filippo Giunchedi <filippo@debian.org>  Sat, 04 Aug 2007 09:22:55 +0200

pybluez (0.9.1-1) unstable; urgency=high

  [ Albert Huang ]
  * New upstream release

  [ Filippo Giunchedi ]
  * this new release (closes: #392183, #392199, FTBFS/RC thus urgency=high)

 -- Filippo Giunchedi <filippo@debian.org>  Wed, 11 Oct 2006 11:44:21 +0200

pybluez (0.9-1) unstable; urgency=low

  * New upstream release

 -- Albert Huang <albert@csail.mit.edu>  Sat,  9 Sep 2006 13:41:27 -0400

pybluez (0.7.1-3) unstable; urgency=low

  * build-depend on python-central and move dh_pycentral before dh_python call
  * oops, forgot ${shlibs:Depends}
  * get replaces/conflicts right  (Closes: #376314)

 -- Filippo Giunchedi <filippo@debian.org>  Sun,  2 Jul 2006 14:22:47 +0200

pybluez (0.7.1-2) unstable; urgency=low

  * migration to new python policy, I hope to have got this right!
    (Closes: #373485)
  * build-depend on libbluetooth-dev not libbluetooth1-dev

 -- Filippo Giunchedi <filippo@debian.org>  Thu, 15 Jun 2006 15:37:57 +0200

pybluez (0.7.1-1) unstable; urgency=low

  * New upstream release

 -- Albert Huang <albert@csail.mit.edu>  Fri, 26 May 2006 16:19:56 -0400

pybluez (0.6.1-2) unstable; urgency=low

  * wow! this closes the python-bluetooth ITP (Closes: #349705)
  * use virtual packages to comply with python policy, thus python{,2.x}-bluez
    provides/conflicts python{,2.x}-bluetooth

 -- Filippo Giunchedi <filippo@debian.org>  Tue, 28 Feb 2006 12:29:28 +0100

pybluez (0.6.1-1) unstable; urgency=low

  * New upstream release

 -- Albert Huang <albert@csail.mit.edu>  Fri, 24 Feb 2006 15:50:45 -0500

pybluez (0.6-1) UNRELEASED; urgency=low

  * New upstream release

 -- Albert Huang <albert@csail.mit.edu>  Sat, 18 Feb 2006 15:54:10 -0500

pybluez (0.5-1) UNRELEASED; urgency=low

  * New upstream release

 -- Albert Huang <albert@csail.mit.edu>  Fri, 16 Dec 2005 16:57:58 -0500

pybluez (0.4-1) unstable; urgency=low

  * new upstream release.

 -- Albert Huang <albert@csail.mit.edu>  Wed,  9 Nov 2005 14:14:13 -0500

pybluez (0.3-4) unstable; urgency=low

  * making the package debian-compliant.

 -- Albert Huang <albert@csail.mit.edu>  Fri,  4 Nov 2005 01:01:52 -0500

pybluez (0.3-3) unstable; urgency=low

  * fixed dependency error (depended on python-2.4 instead of python2.4)

 -- Albert Huang <albert@csail.mit.edu>  Thu, 27 Oct 2005 12:48:00 -0400

pybluez (0.3-2) unstable; urgency=low

  * reduced dependency versions so that package will install on sarge

 -- Albert Huang <albert@csail.mit.edu>  Fri, 23 Sep 2005 00:18:00 -0400

pybluez (0.3-1) unstable; urgency=low

  * new upstream versoin

 -- Albert Huang <albert@csail.mit.edu>  Tue, 20 Sep 2005 19:18:00 -0400

pybluez (0.2-1) unstable; urgency=low

  * Initial Release.
  * My first debian package.

 -- Albert Huang <albert@csail.mit.edu>  Mon,  4 Apr 2005 01:22:15 -0400
